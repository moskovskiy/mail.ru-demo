doc = require("document")

box.cfg{}

box.schema.space.create("key_value", {if_not_exists = true})
doc.create_index(box.space.key_value, "primary", {parts={"key", "string"}, if_not_exists=true})

function deleteKey (key)
    doc.delete(box.space.key_value, {{"$key", "==", key}})
end

function insertKey (key, value)
    doc.insert(box.space.key_value, {key = key, value = value})
end

function selectKey (key) 
    return  doc.select(box.space.key_value, {{"$key", "==", key}})
end

insertKey("Hello", "Hello, Mail.ru!")
