box.cfg{listen = 3301}

KVSpace = box.schema.space.create('KVSpace')

KVSpace:format({
        {name = 'key', type = 'string'},
        {name = 'value', type = 'string'}
})

KVSpace:create_index('primary', {
    type = 'tree',
    parts = {'key'}
})
