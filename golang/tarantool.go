package main

/*
	Complete library is
	avaliable at https://gitlab.com/moskovskiy/tarantools/
*/

import (
	"log"

	"github.com/tarantool/go-tarantool"
)

// Readability sugar
type Any = interface{}
type Tuple = []Any
type Dictionary = map[string]Any

const T__tarantoolDebugEnabled = false

// Tarantool represents tarantool db connection
type Tarantool struct {
	options    tarantool.Opts
	connection *tarantool.Connection
	//schema     *tarantool.Connection.Schema
}

// Connect allows to initialize Tarantool with connection
func (t *Tarantool) Connect(address string, options tarantool.Opts) {
	t.options = options
	connection, err := tarantool.Connect(address, t.options)
	if err != nil {
		if T__tarantoolDebugEnabled {
			log.Println("Debug>TaranTools.go> CONNECTION REFUSED. Tarantool connection refused with error = ", err)
		}
	} else {
		t.connection = connection

		if T__tarantoolDebugEnabled {
			log.Println("Debug>TaranTools.go> Connection succesful with connection =", connection)
		}
	}
}

//PrintSpacesList Debug function
func (t *Tarantool) PrintSpacesList() {
	//log.Printf("Debug> TaranTools.go> List of spaces in connection:\n")
	for _, values := range t.connection.Schema.Spaces {
		log.Print("Debug> TaranTools.go> id: ", values.Id, ", name: ", values.Name)
	}
}

//ExecuteLua executes plain lua code
func (t *Tarantool) ExecuteLua(code string) []interface{} {
	resp, err := t.connection.Eval(code, []interface{}{})

	if T__tarantoolDebugEnabled {
		log.Println("Debug> TaranTools.go> Exectuted :`", code, "`")
		log.Println("Debug> TaranTools.go> Tarantool response> Error = ", err)
		log.Println("Debug> TaranTools.go> Tarantool response> Code = ", resp.Code)
		log.Println("Debug> TaranTools.go> Tarantool response> Data = ", resp.Data)
	}

	return resp.Data
}

//TarantoolSpace is a table in Tarantool
type TarantoolSpace struct {
	name   string
	server *Tarantool
}

//LoadSpace loads space by its name (sort of)
func (s *TarantoolSpace) LoadSpace(t *Tarantool, space string) {
	s.name = space
	s.server = t
}

//ConnectTarantool adds tarantool connection to space var
func (s *TarantoolSpace) ConnectTarantool(t *Tarantool) {
	s.server = t
}

//InitializeSpace creates new spaces
func (s *TarantoolSpace) InitializeSpace(spaceName string, schema string) bool {
	s.name = spaceName
	resp, err := s.server.connection.Eval(`box.schema.create_space('`+spaceName+`')`, []interface{}{})

	if T__tarantoolDebugEnabled {
		log.Println("Debug> TaranTools.go> Added space")
		log.Println("Debug> TaranTools.go> Tarantool response> Error = ", err)
		log.Println("Debug> TaranTools.go> Tarantool response> Code = ", resp.Code)
		log.Println("Debug> TaranTools.go> Tarantool response> Data = ", resp.Data)
	}

	resp, err2 := s.server.connection.Eval(`box.space.`+s.name+`:format({`+schema+`})`, []interface{}{})

	if T__tarantoolDebugEnabled {
		log.Println("Debug> TaranTools.go> Added space format")
		log.Println("Debug> TaranTools.go> Tarantool response> Error = ", err2)
		log.Println("Debug> TaranTools.go> Tarantool response> Code = ", resp.Code)
		log.Println("Debug> TaranTools.go> Tarantool response> Data = ", resp.Data)
	}

	return (err == nil) && (err2 == nil)
}

//InitializeRemoteSpace does something
func (s *TarantoolSpace) InitializeRemoteSpace(t *Tarantool, spaceName string, schema string) {
	s.ConnectTarantool(t)
	s.InitializeSpace(spaceName, schema)
}

//NewIndex adds index to space
func (s *TarantoolSpace) NewIndex(indexName string, structure string) string {
	if s.server == nil {
		if T__tarantoolDebugEnabled {
			log.Println("Debug>TaranTools.go>NewIndex won't work. THIS SPACE IS NOT CONNECTED TO TARANTOOL SPACE")
		}
		return ""
	}

	resp, err := s.server.connection.Eval(`box.space.`+s.name+`:create_index ('`+indexName+`', {
		`+structure+`
		})`, []interface{}{})

	if T__tarantoolDebugEnabled {
		log.Println("Debug> TaranTools.go> Added index")
		log.Println("Debug> TaranTools.go> Tarantool response> Error = ", err)
		log.Println("Debug> TaranTools.go> Tarantool response> Code = ", resp.Code)
		log.Println("Debug> TaranTools.go> Tarantool response> Data = ", resp.Data)
	}

	return indexName
}

// GetIndex does nothing - it returns same string as inserted, but it helps with debugging by checking if DB is connected
func (s *TarantoolSpace) GetIndex(indexName string) string {
	if s.server == nil {
		if T__tarantoolDebugEnabled {
			log.Println("Debug>TaranTools.go>GetIndex won't work. THIS SPACE IS NOT CONNECTED TO TARANTOOL SPACE")
		}
		return ""
	}

	return indexName
}

const tarantoolDuplicateKey uint32 = 3

//Add inserts line into space
func (s *TarantoolSpace) Add(line []interface{}) uint32 {
	if s.server == nil {
		//log.Println("Debug>TaranTools.go>Add won't work.THIS SPACE IS NOT CONNECTED TO TARANTOOL SPACE")
		return 1
	}

	resp, err := s.server.connection.Insert(s.name, line)

	if T__tarantoolDebugEnabled {
		log.Println("Debug> TaranTools.go> Inserted ", line)
		log.Println("Debug> TaranTools.go> Tarantool response> Error = ", err)
		log.Println("Debug> TaranTools.go> Tarantool response> Code = ", resp.Code)
		log.Println("Debug> TaranTools.go> Tarantool response> Data = ", resp.Data)
	}

	return resp.Code
}

//Get selects row with query
func (s *TarantoolSpace) Get(start uint32, offset uint32, iterator uint32, index string, query []interface{}) []interface{} {
	if s.server == nil {
		if T__tarantoolDebugEnabled {
			log.Println("Debug>TaranTools.go>Get won't work. THIS SPACE IS NOT CONNECTED TO TARANTOOL SPACE")
		}
		return nil
	}

	resp, err := s.server.connection.Select(s.name, index, start, offset, iterator, query)

	if T__tarantoolDebugEnabled {
		log.Println("Debug>TaranTools.go> Selected")
		log.Println("Debug> TaranTools.go> Error", err)
		log.Println("Debug> TaranTools.go> Code", resp.Code)
		log.Println("Debug> TaranTools.go> Data", resp.Data)
	}

	return resp.Data
}

// GetAll selects everything
func (s *TarantoolSpace) GetAll(index string) []interface{} {
	if s.server == nil {
		if T__tarantoolDebugEnabled {
			log.Println("Debug>TaranTools.go>GetAll won't work. THIS SPACE IS NOT CONNECTED TO TARANTOOL SPACE")
		}
		return nil
	}

	return s.Get(0, 4294967295 /*tarantool.KeyLimit*/, tarantool.IterGt, index, []interface{}{""})
}

// GetElementByID allows you to get element by unique string id, like in random_id.go
func (s *TarantoolSpace) GetElementByID(id string, index string) []interface{} {
	if s.server == nil {
		if T__tarantoolDebugEnabled {
			log.Println("Debug>TaranTools.go>GetElementByID won't work. THIS SPACE IS NOT CONNECTED TO TARANTOOL SPACE")
		}
		return nil
	}

	return s.Get(0, 1, tarantool.IterEq, index, []interface{}{id})
}

// Truncate clears all the []interface{}s from space. It's is strongly recommended to never use this function
// Proceed with caution
func (s *TarantoolSpace) Truncate() {
	if s.server == nil {
		if T__tarantoolDebugEnabled {
			log.Println("Debug>TaranTools.go>Truncate won't work. THIS SPACE IS NOT CONNECTED TO TARANTOOL SPACE")
		}
		return
	}

	resp, err := s.server.connection.Call("box.space."+s.name+":truncate", []interface{}{})

	if T__tarantoolDebugEnabled {
		log.Println("Debug>TaranTools.go>WARNING! THIS PERMANENTLY DELETES ALL DATA INSIDE OF THE SPACE")
		log.Println("Debug> TaranTools.go> Error", err)
		log.Println("Debug> TaranTools.go> Code", resp.Code)
		log.Println("Debug> TaranTools.go> Data", resp.Data)
	}
}

// Delete row
func (s *TarantoolSpace) Delete(id string, index string) bool {
	if s.server == nil {
		if T__tarantoolDebugEnabled {
			log.Println("Debug>TaranTools.go>Delete won't work. THIS SPACE IS NOT CONNECTED TO TARANTOOL SPACE")
		}
		return false
	}

	resp, err := s.server.connection.Delete(s.name, index, []interface{}{id})

	if T__tarantoolDebugEnabled {
		log.Println("Debug>TaranTools.go>Action:Delete")
		log.Println("Debug> TaranTools.go> Error", err)
		log.Println("Debug> TaranTools.go> Code", resp.Code)
		log.Println("Debug> TaranTools.go> Data", resp.Data)
	}

	return len(resp.Data) > 0
}

// Replace row
func (s *TarantoolSpace) Replace(line []interface{}) bool {
	resp, err := s.server.connection.Replace(s.name, line)

	if T__tarantoolDebugEnabled {
		log.Println("Debug>TaranTools.go>Action:Delete")
		log.Println("Debug> TaranTools.go> Error", err)
		log.Println("Debug> TaranTools.go> Code", resp.Code)
		log.Println("Debug> TaranTools.go> Data", resp.Data)
	}

	return err == nil
}

// Update row
func (s *TarantoolSpace) Update(id string, index string, element string, operation string, value interface{}) bool {
	resp, err := s.server.connection.Update(s.name, index, []interface{}{id}, []interface{}{[]interface{}{operation, element, value}})

	if T__tarantoolDebugEnabled {
		log.Println("Debug>TaranTools.go>Action:Update")
		log.Println("Debug> TaranTools.go> Error", err)
		log.Println("Debug> TaranTools.go> Code", resp.Code)
		log.Println("Debug> TaranTools.go> Data", resp.Data)
	}

	return len(resp.Data) > 0
}

// SetValueKV - Easing works with key value storage: it should be specificaly "value" column and "primary" index
func (s *TarantoolSpace) SetValueKV(key string, value interface{}) bool {
	resp, err := s.server.connection.Upsert(s.name, []interface{}{key, value}, []interface{}{[]interface{}{"=", "value", value}})

	if T__tarantoolDebugEnabled {
		log.Println("Debug>TaranTools.go>Action:Update")
		log.Println("Debug> TaranTools.go> Error", err)
		log.Println("Debug> TaranTools.go> Code", resp.Code)
		log.Println("Debug> TaranTools.go> Data", resp.Data)
	}

	return err == nil
}
