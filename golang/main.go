package main

import (
	"github.com/tarantool/go-tarantool"
	"time"
)

func main() {
	tarantoolDB := new(Tarantool)
	tarantoolDB.Connect("127.0.0.1:3600", tarantool.Opts{
		Timeout:       3600 * time.Second,
		Reconnect:     1 * time.Second,
		MaxReconnects: 3600,
		User:          "guest",
		Pass:          "",
	})

	HTTPListenAndServe(tarantoolDB)
}
