package main

type Schema struct {
	ID   string `json:"id"`
	Body string `json:"body"`
}

func (schema *Schema) updateKV(db *TarantoolSpace) bool {
	return db.Update(schema.ID, "primary", "value", "=", schema.Body)
}

func (schema *Schema) createKV(db *TarantoolSpace) bool {
	err := db.Add(Tuple{schema.ID, schema.Body})
	return err != tarantoolDuplicateKey
}

func (schema *Schema) deleteKV(db *TarantoolSpace) bool {
	return db.Delete(schema.ID, "primary")
}

func (schema *Schema) selectKV(db *TarantoolSpace) (string, bool) {
	element := db.GetElementByID(schema.ID, "primary")
	json := ""

	for _, element := range element {
		parsed := element.(Tuple)
		json = parsed[1].(string)
		return json, true // no need to search for any other elements
	}

	return "", false
}
