require "space"

json = require("json")
log = require("log")

function getRequest(req)
    local key = req:stash("key")

    for _, r in selectKey(key) do
        log.info("GET request: key "..key.." exists")
        return {status = 200, body = json.encode(r)}
    end

    log.info("GET request: key "..key.." doesn`t belong to DB")
    return {status = 404, body = "This key does not belong to DB"}
end


function deleteRequest(req)
    local key = req:stash("key")
    
    for _, r in selectKey(key) do
        log.info("DELETE request: key "..key.." exists")
        deleteKey(key)
        return {status = 200, body = "Deleted"}
    end

    log.info("DELETE request: key "..key.." doesn`t belong to DB")
    return {status = 404, body = "This key does not belong to DB"}
end


function putRequest(req)
    local key = req:stash("key")
    local value

    if pcall(req.json, req) then
        value = req:json()["value"]
        
        log.info("PUT body is valid for key ="..key)

        for _, r in selectKey(key) do
            deleteKey(key)
            insertKey(key, value)
            return {status = 200, body = json.encode(value)}
        end

        log.info("PUT request: key "..key.." doesn`t belong to DB")
        return {status = 404, body = "This key does not belong to DB"}

    else
        log.info("ERROR: PUT body is invalid for key "..key)
        return {status = 400, body = "POST request got error decoding data: check if body is correct JSON format"}
    end
end

function postRequest(req)
    local key
    local value

    if pcall(req.json, req) then
        key = req:json()["key"]
        value = req:json()["value"]
        
        log.info("POST body is valid for key "..key)
        
        insertKey (key, value)
    else
        log.info("ERROR: POST body is invalid for key "..key)
        return {status = 400, body = "POST request got error decoding data: check if body is correct JSON format"}
    end
end
