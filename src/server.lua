require "model"

local http_router = require("http.router")
local http_server = require("http.server")

local httpd = http_server.new("192.168.0.2", 443, {
    log_requests = true,
    log_errors = true
})

local router = http_router.new()

router:route({method = "PUT", path = "/kv/:key/"}, putRequest)
router:route({method = "POST", path = "/kv/"}, postRequest)
router:route({method = "GET", path = "/kv/:key/"}, getRequest)
router:route({method = "DELETE", path = "/kv/:key"}, deleteRequest)

httpd:set_router(router)
httpd:start()