package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"io/ioutil"
	"log"
	"net/http"
)

type IO struct {
	db     *TarantoolSpace
	schema *Schema
}

type Message struct {
	Key   string                 `json:"key"`
	Value map[string]interface{} `json:"value"`
}

func (links *IO) addValue(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)

	links.schema = new(Schema)

	payload := string(reqBody)

	msg := new(Message)

	if json.Unmarshal([]byte(payload), &msg) != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	links.schema.ID = msg.Key
	distilledBody, _ := json.Marshal(msg.Value)
	links.schema.Body = string(distilledBody)

	log.Println("POST succeeded on key = ", links.schema.ID, ", with body = ", links.schema.Body)

	if !links.schema.createKV(links.db) {
		w.WriteHeader(http.StatusConflict)
		log.Println("Create: duplicate key found")
	} else {
		w.WriteHeader(http.StatusOK)
		log.Println("Create: succesful")
	}
}

func (links *IO) editValue(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)

	links.schema = new(Schema)

	payload := string(reqBody)

	msg := new(Message)

	if json.Unmarshal([]byte(payload), &msg) != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	vars := mux.Vars(r)
	links.schema.ID = vars["id"]

	distilledBody, _ := json.Marshal(msg.Value)
	links.schema.Body = string(distilledBody)

	log.Println("PUT succeeded on key = ", links.schema.ID, ", with body = ", links.schema.Body)

	if !links.schema.updateKV(links.db) {
		w.WriteHeader(http.StatusNotFound)
		log.Println("Update: key not found")
	} else {
		w.WriteHeader(http.StatusOK)
		log.Println("Update: succesful")
	}
}

func (links *IO) deleteValue(w http.ResponseWriter, r *http.Request) {
	reqBody, _ := ioutil.ReadAll(r.Body)

	vars := mux.Vars(r)
	key := vars["id"]

	links.schema = new(Schema)
	links.schema.ID = vars["id"]

	log.Println("GET at key: "+links.schema.ID+" succesful with body = ", reqBody)

	if !links.schema.deleteKV(links.db) {
		w.WriteHeader(http.StatusNotFound)
		log.Println("DELETE at key: Could not find value at key")
	} else {
		w.WriteHeader(http.StatusOK)
		log.Println("DELETE at key: " + key + " succesful")
	}
}

func (links *IO) getValue(w http.ResponseWriter, r *http.Request) {
	log.Println("GET")

	reqBody, _ := ioutil.ReadAll(r.Body)

	vars := mux.Vars(r)

	links.schema = new(Schema)
	links.schema.ID = vars["id"]

	log.Println("GET at key: "+links.schema.ID+" succesful with body = ", reqBody)

	resp, exists := links.schema.selectKV(links.db)

	if !exists {
		w.WriteHeader(http.StatusNotFound)
		log.Println("GET at key: Could not find value at key")
	} else {
		log.Println("GET at key: Model responded with = ", resp)
	}

	fmt.Fprintf(w, resp)
}

func HTTPListenAndServe(conn *Tarantool) {
	log.Printf("Listening...")

	io := new(IO)
	io.db = new(TarantoolSpace)
	io.db.LoadSpace(conn, "KVSpace")

	myRouter := mux.NewRouter().StrictSlash(true)

	myRouter.HandleFunc("/kv", io.addValue).Methods("POST")
	myRouter.HandleFunc("/kv/{id}", io.getValue).Methods("GET")
	myRouter.HandleFunc("/kv/{id}", io.editValue).Methods("PUT")
	myRouter.HandleFunc("/kv/{id}", io.deleteValue).Methods("DELETE")

	log.Fatal(http.ListenAndServe(":80", myRouter))
}
