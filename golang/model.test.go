package main

import (
	"github.com/tarantool/go-tarantool"
	"log"
	"time"
)

func TestModel() {
	conn := new(Tarantool)
	conn.Connect("127.0.0.1:3600", tarantool.Opts{
		Timeout:       3600 * time.Second,
		Reconnect:     1 * time.Second,
		MaxReconnects: 3600,
		User:          "guest",
		Pass:          "",
	})

	schema := new(Schema)
	schema.ID = "abcd"
	schema.Body = `{TEST}`

	db := new(TarantoolSpace)
	db.LoadSpace(conn, "KVSpace")

	if !schema.updateKV(db) {
		log.Println("PASS: true , Could not find the key to update")
	}

	if !schema.createKV(db) {
		log.Println("PASS: false , Duplicate code exists")
	}

	resp, exists := schema.selectKV(db)
	log.Println("PASS:", resp == schema.Body, ", Result = ", resp, " exists = ", exists)

	schema.ID = "abcd"
	schema.Body = `{GEGT}`

	if !schema.updateKV(db) {
		log.Println("PASS: false , Could not find the key to update")
	}

	resp, exists = schema.selectKV(db)
	log.Println("PASS:", resp == schema.Body, ", Result = ", resp, " exists = ", exists)

	if !schema.deleteKV(db) {
		log.Println("PASS: false , Could not find the key to delete")
	}

	if !schema.deleteKV(db) {
		log.Println("PASS: true , Could not find the key to delete")
	}

	resp, exists = schema.selectKV(db)
	log.Println("PASS:", exists == false, ", Result = ", resp, " exists = ", exists)
}

func main() {
	TestModel()
}
