# mailru-demo
This is a Tarantool KV Storage backend hiring test for Mail.ru 

API description:
```mrgdemo.moskovskiy.org/kv/{id}```

### Commands
- POST /kv body: {key: "test", "value": {SOME ARBITRARY JSON}} 
- PUT kv/{id} body: {"value": {SOME ARBITRARY JSON}} 
- DELETE kv/{id} 

### Queries
- GET kv/{id} 

### Status codes
- POST returns 409 if key already exists
- POST, PUT returns 400 if body is invalid
- PUT, GET, DELETE returns 404 if key doesn't exist